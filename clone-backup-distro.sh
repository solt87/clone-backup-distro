#!/bin/sh
## Download the backup-distro files from github.

URL="https://gitlab.com/solt87"
REPOS="backup-home mydebianlive mypackages postinstall"

## Create backup-distro/ directory:
cd ~
mkdir backup-distro
cd backup-distro/

## The actual cloning into backup-distro/:
for repo in $REPOS
do
    git clone $URL/$repo
done

exit 0
